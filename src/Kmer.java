import java.util.ArrayList;

/**
 * Created by Jonathan on 2017-11-07.
 */
public class Kmer {

    public static ArrayList<String> trouverKmers(String segment, int k){

        ArrayList<String> kmers = new ArrayList<String>(0);

        int l = segment.length()-k+1;

        for(int i=0; i < l; i++)
            kmers.add(segment.substring(i,i+k));

        return kmers;
    }
}
