import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;
import java.util.ArrayList;

/**
 * TP-3 IFT-3295 Automne 2017 
 * Université de Montréal
 * 
 * JONATHAN LAROSE
 * DENNIS OROZCO MARTINEZ
 */

public class ParserFasta {

    public static ArrayList<String[]> parseBaseDeDonnees(String path){

        ArrayList<String[]> sequences = new ArrayList<String[]>(0);

        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line = br.readLine();

            while(line != null){

                if(line.startsWith(">")) {

                    String[] seq = new String[2];
                    seq[0] = line;
                    seq[1] = br.readLine();

                    sequences.add(seq);
                }

                line = br.readLine();
            }

        }catch(Exception e){
            System.out.println(e);
        }

        return sequences;
    }

}
