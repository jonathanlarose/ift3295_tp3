import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * TP-3 IFT-3295 Automne 2017 
 * Université de Montréal
 * 
 * JONATHAN LAROSE
 * DENNIS OROZCO MARTINEZ
 */

public class Plast {

    public static void main(String[] args){

        /**
         * MISE EN PLACE
         * Setup des variables global avec les arguments en entrée du programme
         */
        //Argument en entrée
        String segment = args[0];
        String pathBaseDeDonnees = args[1];
        ArrayList<String[]> baseDeDonnees = ParserFasta.parseBaseDeDonnees(pathBaseDeDonnees);

        //Paramètre par défaut
        double E = 4;
        double ss = 0.001;
        String seed = "11111111111";
        //String seed = "11110011111";

        //Arguments Optionnels
        for(int i=2; i<args.length; i++){

            String[] arg = args[i].split(":");

            switch (arg[0]){
                case("E"):
                    E = Double.parseDouble(arg[1]);
                    break;
                case("ss"):
                    ss = Double.parseDouble(arg[1]);
                    break;
                case("seed"):
                    seed = arg[1];
                    break;
            }
        }

        //Setup des valeur statiques (constantes) dans les classes
        Hsp.E = E;
        Hsp.ss = ss;
        Hsp.sMatch = 5;
        Hsp.sMismatch = -4;

        //Taille totale de la base de donnée
        Hsp.tailleTotalBD = 0;

        for(String[] s : baseDeDonnees){
            Hsp.tailleTotalBD += s[1].length();
        }

        //Trouver les kmers du segment en input
        HotSpotPairs.segment = segment;
        HotSpotPairs.kmers = Kmer.trouverKmers(segment, seed.length());
        HotSpotPairs.seed = seed;

        /**
         * ALGORITHME
         */
        //Pour chaque sequence dans la base de donnée, on calcule les hot spots pairs
        ArrayList<HotSpotPairs> results = new ArrayList<HotSpotPairs>(0);

        for(String seq[] : baseDeDonnees){

            results.add(new HotSpotPairs(seq));
        }

        for(HotSpotPairs h : results) {
            h.mergeChevauchement();
        }

        //Trouver le hsp avec le meilleur score
        HotSpotPairs bestHotSpotPair = null;
        double bestBitscore = 0;

        for(HotSpotPairs h : results){

            h.findBest();


            if(h.getBestHsp() == null)
                continue;

            if(bestBitscore < h.getBestHsp().getBitscore()){

                bestBitscore = h.getBestHsp().getBitscore();
                bestHotSpotPair = h;
            }
        }

        /**
         * Pour comparer et sort une liste de HotSpotPair
         */
        Collections.sort(results, new Comparator<HotSpotPairs>() {
            @Override
            public int compare(HotSpotPairs o1, HotSpotPairs o2) {
                if(o1.getBestHsp() == null)
                    return -1;

                if(o2.getBestHsp() == null)
                    return 1;

                    if(-o1.getBestHsp().getBitscore() > -o2.getBestHsp().getBitscore())
                        return 1;

                    else if(-o1.getBestHsp().getBitscore() < -o2.getBestHsp().getBitscore())
                        return -1;

                return 0;
            }
        });

        //Print l'alignement et le hsp
        int total = 0;

        for(HotSpotPairs h : results){

            h.findBest();

            if(h.getBestHsp() == null)
                continue;

            System.out.print(h);
            System.out.print("\n-------------------------------------\n\n");

            total++;
        }

        System.out.println("total : " + total);
    }
}
