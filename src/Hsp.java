/**
 * Created by Jonathan on 2017-11-08.
 */
public class Hsp {

    public static double E;
    public static double ss;
    public static int tailleTotalBD;
    public static int sMatch;
    public static int sMismatch;

    private String alignement;
    private int positionSequence;
    private int positionSegment;
    private int score;
    private double bitscore;
    private double eValue;

    //Getters
    public double getBitscore() {
        return bitscore;
    }

    public double geteValue() {
        return eValue;
    }

    public int getPositionSegment() {
        return positionSegment;
    }

    public int getPositionSequence() {
        return positionSequence;
    }

    public int getScore() {
        return score;
    }

    public String getAlignement() {
        return alignement;
    }

    public int length(){
        return alignement.length();
    }

    //Constructeur
    public Hsp(String kmer, int positionSequence, int positionSegment, String sequence, String segment){

        this.alignement = kmer;
        this.positionSequence = positionSequence;
        this.positionSegment = positionSegment;

        calculScore(sequence);
        extension(sequence, segment);
        calculBitScore();
        calculEvalue();
    }

    //Constructeur quand on connait déjà l'alignement (quand on a merge 2 hotspot)
    public Hsp(String merge, int positionSequence, int positionSegment, String sequence){

        this.alignement = merge;
        this.positionSequence = positionSequence;
        this.positionSegment = positionSegment;

        calculScore(sequence);
        calculBitScore();
        calculEvalue();
    }

    /**
     * Fonction qui trouve l'extension du hsp maximisant le score
     * @param sequence
     * @param segment
     */
    public void extension(String sequence, String segment){

        boolean left = true;
        boolean right = true;

        int l = this.alignement.length();
        int max = this.score;
        int sc = this.score;
        int ext_l = 0;
        int ext_r = 0;

        //Fait l'extension jusqu'à ce qu'on dépasse le seuil ou on atteingne les "bords" de la sequence / segment
        while(left || right){

            int tmpL = -1000;
            int tmpR = -1000;
            int tmpB = -1000;

            //Test l'extension de chaque côté
            //left
            if(left){
                if(this.positionSequence + ext_l == 0 || this.positionSegment + ext_l == 0) {
                    left = false;
                }
                else {
                    char c1 = sequence.charAt(this.positionSequence + ext_l - 1);
                    char c2 = segment.charAt(this.positionSegment + ext_l - 1);
                    tmpL = scoreMatch(c1, c2);
                }
            }

            //right
            if(right){
                if(this.positionSequence + l + ext_r >= sequence.length()-1 || this.positionSegment + l + ext_r >= segment.length()-1) {
                    right = false;
                }
                else {
                    char c1 = sequence.charAt(this.positionSequence + l + ext_r);
                    char c2 = segment.charAt(this.positionSegment + l + ext_r);

                    tmpR = scoreMatch(c1, c2);
                }
            }

            //both
            if(left && right)
                tmpB = tmpL + tmpR;

            //Si on ne peut plus faire l'extension d'aucun côté
            if(!left && !right)
                break;


            //Trouver l'extension qui donne le score max
            char ext;
            if(tmpL == tmpR)
                ext = Math.random() > 0.5? 'l' : 'r';
            else
                ext = tmpL > tmpR? 'l' : 'r';

            if(ext == 'l')
                ext = tmpB > tmpL? 'b' : ext;
            else
                ext = tmpB > tmpR? 'b' : ext;


            //Applique l'extension qui maximise le score
            switch (ext){
                case 'l':
                    ext_l--;
                    sc += tmpL;
                    break;
                case 'r':
                    ext_r++;
                    sc += tmpR;
                    break;
                case'b':
                    ext_l--;
                    ext_r++;
                    sc += tmpB;
                    break;
            }

            //Si le nouveau score après extension est meilleur ou égale au score précédent
            if(sc >= max)
                max = sc;

            //Si on dépasse le threshold
            if(E <= max-sc) {
                switch (ext){
                    case 'l':
                        ext_l++;
                        break;
                    case 'r':
                        ext_r--;
                        break;
                    case'b':
                        ext_l++;
                        ext_r--;
                        break;
                }
                break;
            }
        }

        //Update les attributs de hsp avec l'extension
        this.positionSequence += ext_l;
        this.positionSegment += ext_l;
        this.score = max;

        this.alignement = segment.substring(positionSegment, positionSegment - ext_l + l + ext_r);

        calculBitScore();
        calculEvalue();
    }

    /**
     *
     * @param c1
     * @param c2
     * @return
     */
    private int scoreMatch(char c1, char c2){

        if(c1 == c2)
            return sMatch;
        else
            return sMismatch;
    }

    /**
     * Fonction qui calcule le score brut du hsp
     * @param sequence
     */
    private void calculScore(String sequence){

        int s = 0;

        for(int i=0; i<alignement.length(); i++)
            s += (sequence.charAt(positionSequence +i)==alignement.charAt(i))? sMatch : sMismatch;

        this.score = s;
    }

    /**
     * Calcul le bitScore du hsp
     */
    private void calculBitScore(){

        double lambda = 0.192;
        double k = 0.176;

        double b = Math.round((lambda*this.score - Math.log(k)) / Math.log(2));

        this.bitscore = b;
    }

    /**
     * Calcul le e-value du hsp
     */
    private void calculEvalue(){

        this.eValue = tailleTotalBD * HotSpotPairs.segment.length() * Math.pow(2,-this.bitscore);
    }

    /**
     * Fonction static qui merge 2 hsp et retourne un nouveau hsp si c'est possible
     * @param h1
     * @param h2
     * @return  Retourne un nouveau hsp à partir des 2 hsp merger, null si pas possible.
     */
    public static Hsp merge(Hsp h1, Hsp h2, String sequence){

        int pSeg1 = h1.getPositionSegment();
        int pSeg2 = h2.getPositionSegment();
        int pSequence1 = h1.getPositionSequence();
        int pSequence2 = h2.getPositionSequence();

        //cas h1 et h2 commencent ou finissent au même endroit : un inclus dans l'autre
        if(pSeg1 == pSeg2 || pSeg1 + h1.length() == pSeg2 + h2.length()){

            //Retourne le plus long
            return (h1.length() > h2.length())? h1 : h2;
        }

        //Sinon
        if(pSeg1 < pSeg2){

            //Si h1 couvre h2
            if(pSeg1 + h1.length() > pSeg2 + h2.length())
                return h1;

            //chevauchement de segment
            int chevSegment = pSeg1 + h1.length() - pSeg2;
            int chevSequence = pSequence1 + h1.length() - pSequence2;

            //Si les 2 sequences se chevauchent
            if(chevSegment == chevSequence && chevSegment > 0){

                String merge = h1.getAlignement() + h2.getAlignement().substring(chevSegment);
                return new Hsp(merge, pSequence1, pSeg1, sequence);
            }

        }
        else{
            //Si h1 couvre h2
            if(pSeg1 + h1.length() < pSeg2 + h2.length())
                return h2;

            //chevauchement de segment
            int chevSegment = pSeg2 + h2.length() - pSeg1;
            int chevSequence = pSequence2 + h2.length() - pSequence1;

            //Si les 2 sequences se chevauchent
            if(chevSegment == chevSequence && chevSegment > 0) {

                String merge = h2.getAlignement() + h1.getAlignement().substring(chevSegment);
                return new Hsp(merge, pSequence2, pSeg2, sequence);
            }
        }

        //Sinon, pas de chevauchement ou  pas mergeable...
        return null;
    }

    /**
     * Fonction pour retirer les extremites du hsp qui ne match pas du à l'extension, après merge de chevauchement
     * @param sequence
     */
    public void cleanExtension(String sequence){

        while(alignement.charAt(0) != sequence.charAt(positionSequence)){

            alignement = alignement.substring(1);
            this.positionSequence++;
            this.positionSegment++;
        }

        while(alignement.charAt(alignement.length()-1) != sequence.charAt(positionSequence + alignement.length()-1)){

            alignement = alignement.substring(0, alignement.length()-1);
        }

        calculScore(sequence);
        calculBitScore();
        calculEvalue();
    }

    /**
     * Pour print un hsp aligné avec la sequence
     * @return
     */
    public String toString(){

        String s = "";

        for(int i=0; i<positionSequence; i++)
            s += " ";

        s += alignement + "\n";

        return s;
    }
}
