import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

/**
 * TP-3 IFT-3295 Automne 2017 
 * Université de Montréal
 * 
 * JONATHAN LAROSE
 * DENNIS OROZCO MARTINEZ
 */

public class HotSpotPairs{

    public static String segment;
    public static ArrayList<String> kmers;  //kmers : position dans la liste == position dans le segment
    public static String seed;

    private String[] sequence;            //sequence de la Base de Donnée
    private ArrayList<Hsp> hsps;        //Liste des HSPs
    private Hsp bestHsp;
    private AlignementLocal align;

    //Getters
    public String[] getSequence() {
        return sequence;
    }

    public ArrayList<Hsp> getHsps() {
        return hsps;
    }

    public Hsp getBestHsp() {
        return bestHsp;
    }

    //Constructeur
    public HotSpotPairs(String[] sequence){

        this.sequence = sequence;
        this.hsps = new ArrayList<Hsp>(0);

        //Pour chaque kmer dans le segment
        for(int i=0; i<kmers.size(); i++){

            String kmer = kmers.get(i);

            //On cherche les positions où il apparait dans la sequence
            ArrayList positions = AlgoRecherche.rechercheExact(sequence[1], kmer, seed);

            //Pour chaque apparition d'un kmer, on crée un hsp
            for(int j=0; j<positions.size(); j++)
                hsps.add(new Hsp(kmer, (int) positions.get(j), i, sequence[1], segment));
        }

        this.align = new AlignementLocal(sequence[1], segment, 1, -1, -1);
    }

    /**
     * Fonction qui merge les hsps se chevauchant
     */
    public void mergeChevauchement(){

        //Pour chaque hsp dans la liste
        for(int i=0; i<hsps.size(); i++){

            ArrayList<Integer> delete = new ArrayList<Integer>(0);  //Liste des hsp à retirer de la liste (une fois merger à un autre)

            //On le compare aux autres hsps
            for(int j=0; j<hsps.size(); j++){

                if(i==j)
                    continue;

                //Essait de merger les 2 hsps
                Hsp tmp = Hsp.merge(hsps.get(i), hsps.get(j), sequence[1]);

                //Si le merge fonctionne
                if(tmp != null) {
                    //On remplace le hsp 1 par le résultat du merge et on ajoute l'autre à la liste de hsps à retirer
                    hsps.set(i, tmp);
                    delete.add(j);
                }
            }

            //Retire les hsps qui devait être retiré
            for(int k=delete.size()-1; k>=0; k--) {
                if(k <= i)
                    i--;
                hsps.remove((int) delete.get(k));
            }
        }

        for(Hsp h : hsps){
            h.cleanExtension(sequence[1]);
        }
    }

    public void findBest(){

        Hsp best = null;
        double scoreMax = -Integer.MAX_VALUE;

        for(Hsp h : hsps){

            if(h.geteValue() > Hsp.ss)
                continue;

            if(h.getBitscore() > scoreMax)
                best = h;
                scoreMax = h.getBitscore();
        }

        this.bestHsp = best;
    }

    public String toString(){

        String s = "";

        s += sequence[0];
        s += ", score: " + align.getScore() + " ";
        s += "ident: " + align.getIdent() + "\n";

        s += align;
        s += "\n";

        s += bestToString();

        return s;
    }

    public String toStringDebug(){

        String s = "Sequences :\n"+ sequence[1] + "\nhsps :\n";

        if(!hsps.isEmpty()) {

            for (Hsp h : hsps) {
                s += h;
            }
        }

        return s;
    }



    public String bestToString(){

        Hsp b = bestHsp;

        if(b == null)
            return "";

        String s = "# Best HSP:\n";
        s += "id:" + this.sequence[0].substring(1) + ", ";
        s += "score: " + b.getScore() + ", ";
        s += "bitscore: " + b.getBitscore() + ", ";
        s += "evalue: " + b.geteValue() + "\n";

        String seg = b.getAlignement();
        String sq = this.sequence[1].substring(b.getPositionSequence(), b.getPositionSequence()+ seg.length());

        s += b.getPositionSegment() + "\t" + seg + "  " + (b.getPositionSegment() + seg.length()-1) + "\n";
        s += b.getPositionSequence() + "\t" + sq + "  " + (b.getPositionSequence() + seg.length()-1) + "\n";

        return s;
    }

}
