Pour compiler le programme :

	javac *.java

Pour rouler le programme :
Paramètre par défaut :

	java Plast <Sequence> <path du fichier base de donnee .fasta>

Avec paramètre optionnel : (nom du paramètre suivi de ':' et valeur du paramètre (pas d'espace))

	java Plast <Sequence> <path du fichier base de donnee .fasta> E:<valeur E> ss:<valeur ss> seed:<String seed>



Exemple :

	GAAAATCCTCGTGTCACCAGTTCAAATCTGGTTCCTGGCA ..\tRNAs.fasta

	GAAAATCCTCGTGTCACCAGTTCAAATCTGGTTCCTGGCA ..\tRNAs.fasta E:12 ss:0.1 seed:110011

	GAAAATCCTCGTGTCACCAGTTCAAATCTGGTTCCTGGCA ..\tRNAs.fasta ss:0.001



**Sur Mac, le path devrait être entre ''  : '..\tRNAs.fasta'
**Sur windows, le path semble seulement fonctionner en relatif à partir de la ligne de commande...