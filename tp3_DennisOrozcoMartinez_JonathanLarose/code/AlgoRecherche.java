import java.util.ArrayList;

/**
 * TP-3 IFT-3295 Automne 2017 
 * Université de Montréal
 * 
 * JONATHAN LAROSE
 * DENNIS OROZCO MARTINEZ
 */

public class AlgoRecherche {

    /**
     * Fonction qui retourne une liste de toutes les positions ou on trouve le segment dans la sequence en input
     * @param sequence
     * @param segment
     * @return ArrayList de int représentant chaque positions du segment dans la sequence
     */
    public static ArrayList rechercheExact(String sequence, String segment, String seed){

        ArrayList<Integer> positions = new ArrayList<Integer>(0);
        int lSeq = sequence.length();
        int lSeg = segment.length();

        for(int i=0; i<lSeq-lSeg+1; i++){

            int j =0;

            while(j<lSeg){

                if(seed.charAt(j)=='1' && sequence.charAt(i+j) != segment.charAt(j))
                    break;

                j++;
            }

            if(j == lSeg)
                positions.add(i);
        }

        return positions;
    }
}
