/**
 * TP-3 IFT-3295 Automne 2017 
 * Université de Montréal
 * 
 * JONATHAN LAROSE
 * DENNIS OROZCO MARTINEZ
 */

/*
 * Classe qui calcule un alignement local entre deux sequences donnees
 */
public class AlignementLocal {
	
	String seq1;
	String seq2;
	int matchValue;
	int mismatchValue;
	int indelValue;

	private double score;
	private double ident;

	// Sequences a retourner apres l'alignement
    private String alignedSeq1 = "";
    private String alignedSeq2 = "";

    //Getters
    public double getScore(){
        return score;
    }

    public double getIdent(){
        return ident;
    }
	
    /*
     * Constructeur
     */
	AlignementLocal(String seq1, String seq2, int matchValue, int mismatchValue, int indelValue){
		
		this.seq1 = seq1;
		this.seq2 = seq2;
		this.matchValue = matchValue;
		this.mismatchValue = mismatchValue;
		this.indelValue = indelValue;
		aligne();
	}
	/*
	 * Methode qui remplie les deux matrices, de scores et de predecesseurs pour retrouver l'alignement
	 */
	 public void aligne(){
		 
		int lr = seq1.length()+1;
	    int lc = seq2.length()+1;
	    
		int[][] matrixScore = new int[lr][lc];
	    String[][] matrixPred = new String[lr][lc];
	    
	    int posi = 0;
	    int posj = 0;
	    int score = 0;
	    int scoreTmp;

	    for(int i=0; i<lr; i++){
	    	for(int j=0; j<lc; j++){
	    		matrixPred[i][j] = "";

	             //Si on est dans la première rangé ou colonne, on ne fait qu'initialiser à 0 et continuer
	            if(j==0 || i==0) {

	                matrixScore[i][j] = 0;

	                if(i==j)
	                    continue;

	                if(j==0)
	                    matrixPred[i][j] = "u";
	                else
	                    matrixPred[i][j] = "l";

	                continue;
	            }
	             //indel seq1
	            matrixScore[i][j] = matrixScore[i-1][j] + indelValue;	     	            
	            matrixPred[i][j] = "u";

	             //indel seq2
	            scoreTmp = matrixScore[i][j-1] + indelValue;	            

	            if (scoreTmp == matrixScore[i][j])
	                matrixPred[i][j] += "l";

	            else if (scoreTmp > matrixScore[i][j]){
	                matrixScore[i][j] = scoreTmp;
	                
	                matrixPred[i][j] = "l";
	            }

	            //Diagonal : match ou mismatch
	            
	            int p = (seq1.charAt(i-1) == seq2.charAt(j-1))? matchValue : mismatchValue;
	            scoreTmp =  matrixScore[i-1][j-1] + p;

	            if(scoreTmp == matrixScore[i][j])
	            	matrixPred[i][j] += "d";

	            else if (scoreTmp > matrixScore[i][j]) {
	            	
	                matrixScore[i][j] = scoreTmp;	               
	                
	                matrixPred[i][j] = "d";
	            }  
	         
	            // Si jamais le score est negatif on remet a zero la case correspondante
	            
	            if (matrixScore[i][j] < 0)
	            	 matrixScore[i][j] = 0;	            
	         }
	    }
	    
	    // On repere la case ou se trouve le score maximal pour effectuer la recherche
	    
	    for(int i=0; i<lr; i++){
	    	for(int j=0; j<lc; j++){
	    		
	    		if (matrixScore[i][j] > score){
	    			score = matrixScore[i][j];
	    			posi = i;
	    			posj = j;
	    		}
	    	}
	    }

	    retrouverAlignement(matrixPred, matrixScore, posi, posj);

         calculScore();
         calculIdent();
 }

 //Fonction qui retrouve l'alignement des séquence et le chauvechement
	 
 private void retrouverAlignement(String[][] matricePred,int[][] matrixScore, int posi, int posj){

     int i = posi;
     int j = posj;

     String pred = matricePred[i][j];
     char direction;

     while( matrixScore[i][j] != 0) {

         direction = pred.charAt((int) Math.floor(pred.length()*Math.random()));         

         switch(direction){
             case 'u':
                 alignedSeq1 += seq1.charAt(i-1);
                 alignedSeq2 += "-";
                 i--;
                 break;
             case 'l':
                 alignedSeq1 += "-";
                 alignedSeq2 += seq2.charAt(j-1);
                 j--;
                 break;
             case 'd':
                 alignedSeq1 += seq1.charAt(i-1);
                 alignedSeq2 += seq2.charAt(j-1);
                 i--;
                 j--;
                 break;
         }         
         
         pred = matricePred[i][j];
     }
 }

 private void calculScore(){

	 double s = 0;

	 for(int i=0; i<alignedSeq1.length(); i++)
		 s += (alignedSeq1.charAt(i) == alignedSeq2.charAt(i))? matchValue : mismatchValue;

	 this.score = s;
 }

 private void calculIdent(){

     double match = 0;

     for(int i=0; i<alignedSeq1.length(); i++)
         match += (alignedSeq1.charAt(i) == alignedSeq2.charAt(i))? matchValue : 0;

     this.ident = match / alignedSeq1.length() * 100;
 }

 public String toString(){
	 
	 // On reverse les sequences pour l'affichage
	 seq1 = new StringBuffer(alignedSeq1).reverse().toString();
	 seq2 = new StringBuffer(alignedSeq2).reverse().toString();
	 
	 String alignement = seq1 + "\n";
     alignement += seq2 + "\n";
     
     return alignement;
     
     }

}
